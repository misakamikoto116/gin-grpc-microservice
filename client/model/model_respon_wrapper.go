package model

// ResponseWrapper model
type ResponseWrapper struct {
	Success bool        `json:"succes"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}
