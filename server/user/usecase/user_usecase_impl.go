package usecase

import (
	"server-microservice-example/model"
	"server-microservice-example/user"
)

type UserUseCaseImpl struct {
	userRepo user.UserRepo
}

func CreateUserUseCase(userRepo user.UserRepo) user.UserUsecase {
	return &UserUseCaseImpl{userRepo}
}

func (e *UserUseCaseImpl) AddUser(user *model.User) (*model.User, error) {
	return e.userRepo.AddUser(user)
}

func (e *UserUseCaseImpl) FindUserById(id model.UserId) (*model.User, error) {
	return e.userRepo.FindUserById(id)
}

func (e *UserUseCaseImpl) FindUsers() (*[]model.User, error) {
	return e.userRepo.FindUsers()
}

func (e *UserUseCaseImpl) UpdateUser(user *model.UserUpdate) (*model.User, error) {
	return e.userRepo.UpdateUser(user)
}

func (e *UserUseCaseImpl) DeleteUser(id *model.UserId) error {
	return e.userRepo.DeleteUser(id)
}
