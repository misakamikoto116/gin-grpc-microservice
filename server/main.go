package main

import (
	"fmt"
	"log"
	"net"
	"net/url"
	"server-microservice-example/model"
	userHandler "server-microservice-example/user/handler"
	userRepo "server-microservice-example/user/repo"
	userUseCase "server-microservice-example/user/usecase"

	_ "github.com/jinzhu/gorm/dialects/postgres"

	"github.com/jinzhu/gorm"
	"google.golang.org/grpc"
)

func main() {
	var port = "8957"
	dsn := url.URL{
		User:     url.UserPassword("postgres", "123"),
		Scheme:   "postgres",
		Host:     fmt.Sprintf("%s:%d", "localhost", 5434),
		Path:     "user_grpc",
		RawQuery: (&url.Values{"sslmode": []string{"disable"}}).Encode(),
	}

	db, err := gorm.Open("postgres", dsn.String())
	if err != nil {
		log.Fatal(err)
	}

	db.Debug().AutoMigrate(
		model.UserDB{},
	)

	server := grpc.NewServer()

	// Repo
	userRep := userRepo.CreateUserRepoImpl(db)

	// Service / usecase
	userUC := userUseCase.CreateUserUseCase(userRep)

	// Handler
	userHandler.CreateUserHandler(server, userUC)

	conn, err := net.Listen("tcp", ":"+port)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Server Starting at port: ", port)
	log.Fatal(server.Serve(conn))
}
